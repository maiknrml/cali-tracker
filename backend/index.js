const express = require("express");
const bodyParser = require("body-parser");
const app = express();
var cors = require("cors");

app.use(cors());

app.use(bodyParser.json());

const auth = require("./routes/auth.js");
app.use("/", auth);

const skills = require("./routes/skills.js");
app.use("/", skills);

const workouts = require("./routes/workouts.js");
app.use("/", workouts);

app.listen(3000, () => console.log("Server Listening at 3000"));
