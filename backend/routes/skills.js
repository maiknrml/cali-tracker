const express = require("express");
const router = express.Router();
const db = require("../lib/db.js");
const userMiddleware = require("../middleware/users.js");

const app = express();

router.get("/skills", (req, res, next) => {
  db.query(`SELECT * FROM skills`, (err, result) => {
    if (err) {
      throw err;
      return res.status(400).send({
        msg: err
      });
    } else {
      res.send(result);
    }
  });
});

router.post("/skills", (req, res, next) => {
  db.query(
    `INSERT INTO skills (name, value) VALUES ('${req.body.name}', '${req.body.value}')`,
    (err, result) => {
      if (err) {
        throw err;
        return res.status(400).send({
          msg: err
        });
      }
      return res.status(201).send({
        msg: "added skill!"
      });
    }
  );
});

module.exports = router;
