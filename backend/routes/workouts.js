const express = require("express");
const router = express.Router();
const db = require("../lib/db.js");
const userMiddleware = require("../middleware/users.js");

const app = express();

router.get("/workouts", (req, res, next) => {
  db.query(`SELECT * FROM workouts`, (err, result) => {
    if (err) {
      throw err;
      return res.status(400).send({
        msg: err
      });
    } else {
      res.send(result);
    }
  });
});

router.post("/workouts", (req, res, next) => {
  db.query(
    `INSERT INTO workouts (name, time, reps, sets, exercise1, exercise2, exercise3, exercise4, exercise5, exercise6, exercise7, exercise8, exercise9, exercise10) VALUES ('${req.body.name}', '${req.body.time}', '${req.body.reps}', '${req.body.sets}', '${req.body.exercise1}', '${req.body.exercise2}', '${req.body.exercise3}', '${req.body.exercise4}', '${req.body.exercise5}', '${req.body.exercise6}', '${req.body.exercise7}', '${req.body.exercise8}', '${req.body.exercise9}', '${req.body.exercise10}')`,
    (err, result) => {
      if (err) {
        throw err;
        return res.status(400).send({
          msg: err
        });
      }
      return res.status(201).send({
        msg: "added workout!"
      });
    }
  );
});

module.exports = router;
