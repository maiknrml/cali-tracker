import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "/login", component: () => import("pages/Login.vue") },
      { path: "/register", component: () => import("pages/Register.vue") },
      { path: "/overview", component: () => import("pages/Overview.vue") },
      { path: "/profil", component: () => import("pages/Profil.vue") },
      { path: "/settings", component: () => import("pages/Settings.vue") },
      { path: "/workouts", component: () => import("pages/Workouts.vue") }

    ]
  }
  // {
  //   path: "/login",
  //   component: "pages/Login.vue"
  // }
  // {
  //   path: "/register",
  //   component: "pages/Register.vue"
  // }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
